package com.example.myapplication.ult;

public class Global {
    public static final boolean DEBUG =true;
    public static String TOKEN = "CWB-41FDF095-1E59-4353-A672-C677595B0AF2";
    public static final String API_DATA_TAIPEI =
            "https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?locationName=臺北市";
    public static final String API_DATA_KAOHSIUNG =
            "https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?locationName=高雄市";
}
