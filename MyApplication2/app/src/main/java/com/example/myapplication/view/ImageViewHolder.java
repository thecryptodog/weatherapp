package com.example.myapplication.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.dataModel.ImageTypeModel;

import java.util.Map;

public class ImageViewHolder extends BaseViewHolder<ImageTypeModel> {

    public static class Factory extends BaseViewHolder.Factory{

        public Factory(@Nullable ClickFuncBuilder clickFuncBuilder) {
            super(clickFuncBuilder);
        }

        @NonNull
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vh_image, parent, false);
            return new ImageViewHolder(view,clickFuncMap);
        }
        @Override
        public String getType() {
            return ImageTypeModel.TYPE;
        }
    }

    ImageView img;

    protected ImageViewHolder(View itemView, final Map<Integer, ClickHandler> clickHandlerMap) {
        super(itemView);
        img = itemView.findViewById(R.id.img);
    }


    @Override
    public void bind(ImageTypeModel wdm) {
        img.setImageResource(wdm.getImageURL());

    }
}
