package com.example.myapplication.ult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NetworkController {
    private static final String TAG = "NetworkController";
    private static NetworkController networkController;
    private OkHttpClient client;
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse( "image/png");
    private static final MediaType MEDIA_TYPE_JPG = MediaType.parse( "image/jpeg");
    public static final MediaType FORM = MediaType.parse("multipart/form-data");


    private NetworkController() {
        client = new OkHttpClient();
    }

    public static NetworkController getInstance() {
        if (networkController == null) {
            networkController = new NetworkController();
        }
        return networkController;
    }

    private static class CallbackAdapter implements Callback {

        private CCallback cCallback;

        public CallbackAdapter(CCallback cCallback) {
            this.cCallback = cCallback;
        }

        @Override
        public void onFailure(@NotNull Call call, @NotNull IOException e) {
            cCallback.onFailure(e.getMessage());
            cCallback.onCompleted();
        }


        @Override
        public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
            String str = response.body().string();
            try {
                JSONObject jsonObject = new JSONObject(str);
                if (response.isSuccessful()) {
                    cCallback.onResponse(jsonObject);
                } else {
                    cCallback.onFailure(response.code()+"");
                }
            } catch (JSONException e) {
                cCallback.onFailure(e.getMessage());
            } finally {
                cCallback.onCompleted();
            }
        }
    }

    public interface CCallback {
        void onFailure(String errorMsg);//偷來的方法
        void onResponse(JSONObject data) throws JSONException; //偷來的方法
        void onCompleted(); // 橋接的目的方法(額外新增)
    }

    public void get(String ApiCommand, CCallback callback){
        Request request = new Request.Builder()
                .header("Authorization", Global.TOKEN)
                .url(ApiCommand)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new CallbackAdapter(callback));
        return;
    }
}
