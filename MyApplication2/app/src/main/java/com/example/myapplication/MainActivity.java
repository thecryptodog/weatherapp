package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.dataModel.ImageTypeModel;
import com.example.myapplication.dataModel.WeatherDataModel;
import com.example.myapplication.ult.Global;
import com.example.myapplication.ult.Logger;
import com.example.myapplication.ult.NetworkController;
import com.example.myapplication.view.BaseModel;
import com.example.myapplication.view.BaseViewHolder;
import com.example.myapplication.view.ImageViewHolder;
import com.example.myapplication.view.RecycleViewAdaptor;
import com.example.myapplication.view.WeatherViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";
    private RecycleViewAdaptor recycleViewAdapter;
    private ArrayList<BaseModel> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataList = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = findViewById(R.id.rv_weatherlist);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.smoothScrollToPosition(0);

        //建立兩種格式的recycleView
        recycleViewAdapter = new RecycleViewAdaptor(
                Arrays.asList(new ImageViewHolder.Factory(new BaseViewHolder.ClickFuncBuilder()
                                .add(R.id.vh_weatherData, new BaseViewHolder.ClickHandler() {
                                    @Override
                                    public void onClick(int position) {

                                    }
                                })),
                        new WeatherViewHolder.Factory(new BaseViewHolder.ClickFuncBuilder()
                                .add(R.id.vh_weatherData, new BaseViewHolder.ClickHandler() {
                                    @Override
                                    //點擊資料換頁事件
                                    public void onClick(int position) {
                                        WeatherDataModel wdm = (WeatherDataModel)dataList.get(position);
                                        jumpToMain2Activity(wdm);
                                    }
                                }))
                )
        );

        recycleViewAdapter.bindDataSource(dataList);
        recyclerView.setAdapter(recycleViewAdapter);

        //取得資料
        getData();

        //檢測是否為第一次使檢測是否為第一次開啟App
        SharedPreferences sp = getSharedPreferences(getString(R.string.SP_DATA),MODE_PRIVATE);
        if(sp.getInt(getString(R.string.SP_LOG),0) == 0) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt(getString(R.string.SP_LOG), 1)
            .apply();
        }else{
            greeting();
        }


    }
    private void greeting(){
        Toast.makeText(this,getString(R.string.comeback_greeting),Toast.LENGTH_SHORT).show();
    }



    private void getData() {
        NetworkController.getInstance().get(Global.API_DATA_TAIPEI, new NetworkController.CCallback() {
            @Override
            public void onFailure(String errorMsg) {
                Logger.d(TAG,getString(R.string.httpCallError) + errorMsg);
            }

            @Override
            public void onResponse(JSONObject data) throws JSONException {

                //解析JSON格式資料
                JSONObject allData = data.getJSONObject("records");
                String location = allData.getJSONArray("location").getJSONObject(0).getString("locationName");
                JSONObject weatherRecordsMinT = allData
                        .getJSONArray("location")
                        .getJSONObject(0)//只有一個物件
                        .getJSONArray("weatherElement")
                        .getJSONObject(2);//指定MinT資料
                JSONArray timeData = weatherRecordsMinT.getJSONArray("time");

                //導入三筆所需資料
                for (int i = 0; i < 3; i++) {
                    String sTime = timeData.getJSONObject(i).getString("startTime");
                    String eTime = timeData.getJSONObject(i).getString("endTime");
                    String pName = timeData.getJSONObject(i).getJSONObject("parameter").getString("parameterName");
                    String pUnit = timeData.getJSONObject(i).getJSONObject("parameter").getString("parameterUnit");
                    dataList.add(new WeatherDataModel(location, sTime, eTime, pName, pUnit));
                    dataList.add(new ImageTypeModel(R.drawable.ic_android_black_24dp));
                }

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        recycleViewAdapter.notifyDataSetChanged();
                    }
                });

            }

            @Override
            public void onCompleted() {

            }
        });
    }


    private void jumpToMain2Activity(WeatherDataModel data) {
        Intent intent = new Intent(this, Main2Activity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(getString(R.string.weatherDataModel), data);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}
