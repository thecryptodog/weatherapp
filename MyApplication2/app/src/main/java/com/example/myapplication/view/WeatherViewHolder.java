package com.example.myapplication.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.dataModel.WeatherDataModel;

import java.util.Map;

public class WeatherViewHolder extends BaseViewHolder<WeatherDataModel> {

    public static class Factory extends BaseViewHolder.Factory{

        public Factory(@Nullable ClickFuncBuilder clickFuncBuilder) {
            super(clickFuncBuilder);
        }

        @NonNull
        public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vh_weatherdata, parent, false);
            return new WeatherViewHolder(view,clickFuncMap);
        }
        @Override
        public String getType() {
            return WeatherDataModel.TYPE;
        }
    }

    TextView minT;


    protected WeatherViewHolder(View itemView, final Map<Integer, ClickHandler> clickHandlerMap) {
        super(itemView);

        minT = itemView.findViewById(R.id.minT);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickHandlerMap.get(v.getId()).onClick(getLayoutPosition());
            }
        });
    }
    @Override
    public void bind(WeatherDataModel wdm) {
        minT.setText(wdm.getLocation() +"\n"+wdm.getTimeDuration() +"\n"+wdm.getMinT());

    }
}