package com.example.myapplication.dataModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.myapplication.view.BaseModel;

public class WeatherDataModel implements BaseModel, Parcelable {

    public static String TYPE = "weatherDataModel";
    String location;
    String timeDuration;
    String minT;

    public WeatherDataModel(String location, String startTime, String endTime, String pName, String pUnit){
       this.location =location;
        this.timeDuration= startTime+"\n"+ endTime;
        this.minT = pName+pUnit;
    }


    protected WeatherDataModel(Parcel in) {
        location = in.readString();
        timeDuration = in.readString();
        minT = in.readString();
    }

    public static final Creator<WeatherDataModel> CREATOR = new Creator<WeatherDataModel>() {
        @Override
        public WeatherDataModel createFromParcel(Parcel in) {
            return new WeatherDataModel(in);
        }

        @Override
        public WeatherDataModel[] newArray(int size) {
            return new WeatherDataModel[size];
        }
    };

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getMinT() {
        return minT;
    }

    public void setMinT(String minT) {
        this.minT = minT;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(location);
        dest.writeString(timeDuration);
        dest.writeString(minT);
    }
}
