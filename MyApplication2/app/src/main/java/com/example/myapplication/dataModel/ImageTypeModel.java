package com.example.myapplication.dataModel;
import com.example.myapplication.view.BaseModel;

public class ImageTypeModel implements BaseModel {

    int imageURL;

    public ImageTypeModel(int imageURL){
        this.imageURL=imageURL;
    }

    public static String TYPE = "ImageTypeModel";

    public int getImageURL() {
        return imageURL;
    }

    public void setImageURL(int image) {
        this.imageURL = image;
    }

    @Override

    public String getType() {
        return TYPE;
    }
}
