package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.dataModel.WeatherDataModel;

public class Main2Activity extends AppCompatActivity {

    WeatherDataModel wdm;
    TextView tv_wdm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent it = getIntent();
        Bundle bundle = it.getExtras();
        wdm = bundle.getParcelable(getString(R.string.weatherDataModel));
        init();
    }
    private void init(){
        tv_wdm=findViewById(R.id.tv_datatoshow);
        tv_wdm.setText(wdm.getLocation() +"\n"+wdm.getTimeDuration() +"\n"+wdm.getMinT());
    }
}
